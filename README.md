# Koilerplate

[![CircleCI](https://circleci.com/gh/infinitered/ignite-bowser.svg?style=svg)](https://circleci.com/gh/infinitered/ignite-bowser)

## The latest and greatest boilerplate, based on Infinite Red opinions and experience of Khoi Tran

This is the boilerplate that [Infinite Red](https://infinite.red) and @nguyenkhooi uses as a way to test bleeding-edge changes to our React Native stack.

Currently includes:

- React Native
- React Navigation
- MobX State Tree
- TypeScript
- And more!

## Quick Start

The Koilerplate project's structure will look similar to this:

```jsx
koizzz-project
├── app
│   ├── screens
│   ├── assets
│   ├── utils
│   ├── components
│   ├── engines
│   ├── app.tsx
├── storybook
│   ├── views
│   ├── index.ts
│   ├── storybook-registry.ts
│   ├── storybook.ts
├── test
│   ├── __snapshots__
│   ├── storyshots.test.ts.snap
│   ├── mock-i18n.ts
│   ├── mock-reactotron.ts
│   ├── setup.ts
│   ├── storyshots.test.ts
├── README.md
├── android
│   ├── app
│   ├── build.gradle
│   ├── gradle
│   ├── gradle.properties
│   ├── gradlew
│   ├── gradlew.bat
│   ├── keystores
│   └── settings.gradle
├── ignite
│   ├── ignite.json
│   └── plugins
├── index.js
├── ios
│   ├── IgniteProject
│   ├── IgniteProject-tvOS
│   ├── IgniteProject-tvOSTests
│   ├── IgniteProject.xcodeproj
│   └── IgniteProjectTests
├── .env
└── package.json

```

### ./app directory

![Sauce](https://www.simplyrecipes.com/wp-content/uploads/2007/01/basic-tomato-sauce-horiz-a-1200-600x400.jpg)

Included in an Koilerplate project is the `app` directory. Following SAUCE structure: `Screens - Assets - Utils - Components - Engines`, this is a directory you would normally have to create when using vanilla React Native.

The inside of the src directory looks similar to the following:

```jsx
app
├── screens
├── assets
    |_ fonts - Fonts folder of the codebase
    |_ images - Images folder of the codebase
    |_ icons -  Icons folder of the codebase
    |_ svg - SVG folder of the codebase
├── utils
    |_ styles -
    |_ helpers - Useful values for the codebase
    |_ i18n - This is where your translations will live if you are using `react-native-i18n`
    |_ errors -  Pre-defined error code
    |_ typings - Type annotations in TypeScript
├── components
├── engines
    |_ functions
    |_ models - This is where your app's models will live. Read more below
    |_ navigation - This is where your `react-navigation` navigators will live
    |_ services - Any services that interface with the outside world will live here (think REST APIs, Push Notifications, etc.)
├── app.tsx
```

**screens**
This is where your screen components will live. A screen is a React component which will take up the entire screen and be part of the navigation hierarchy. Each screen will have a directory containing the `.tsx` file, along with any assets or other helper files.

**assets**
[TODO complete]

**utils**
This is a great place to put miscellaneous helpers and utilities. Things like date helpers, formatters, etc. are often found here. However, it should only be used for things that are truely shared across your application. If a helper or utility is only used by a specific component or model, consider co-locating your helper with that component or model.

**components**
This is where your React components will live. Each component will have a directory containing the `.tsx` file, along with a story file, and optionally `.presets`, and `.props` files for larger components. The app will come with some commonly used components like Button.

**engines**
[TODO complete]

**app.tsx** This is the entry point to your app. This is where you will find the main App component which renders the rest of the application. This is also where you will specify whether you want to run the app in storybook mode.

**i18n**
This is where your translations will live if you are using `react-native-i18n`.

**models**
This is where your app's models will live. Each model has a directory which will contain the `mobx-state-tree` model file, test file, and any other supporting files like actions, types, etc.

**navigation**
This is where your `react-navigation` navigators will live.

**services**
Any services that interface with the outside world will live here (think REST APIs, Push Notifications, etc.).

**styles**
Here lives the theme for your application, including spacing, colors, and typography.

### ./ignite directory

The `ignite` directory stores all things Ignite, including CLI and boilerplate items. Here you will find generators, plugins and examples to help you get started with React Native.

### ./storybook directory

This is where your stories will be registered and where the Storybook configs will live

### ./test directory

This directory will hold your Jest configs and mocks, as well as your [storyshots](https://github.com/storybooks/storybook/tree/master/addons/storyshots) test file. This is a file that contains the snapshots of all your component storybooks.

## Running Storybook

From the command line in your generated app's root directory, enter `yarn run storybook`
This starts up the storybook server.

In `app/app.tsx`, change `SHOW_STORYBOOK` to `true` and reload the app.

For Visual Studio Code users, there is a handy extension that makes it easy to load Storybook use cases into a running emulator via tapping on items in the editor sidebar. Install the `React Native Storybook` extension by `Orta`, hit `cmd + shift + P` and select "Reconnect Storybook to VSCode". Expand the STORYBOOK section in the sidebar to see all use cases for components that have `.story.tsx` files in their directories.

## Final check for general

:heavy_check_mark: Make sure `react-native.config.js` looks like this:

```js
module.exports = {
  project: {
    ios: {},
    android: {},
  },
  assets: ["./app/assets/fonts"],
}
```

So that when we use `react-native-link`, it JUST link `assets`, not dependencies :+1:


## For Android

### Ignite's Bowser template

:heavy_check_mark: There is the bug in the latest version of Ignite's Bowser template that prevents the app from running on Android ([#1535](https://github.com/infinitered/ignite/issues/1535)). So, make sure in `/app/build.gradle`'s dependencies section', there are these lines:

```
implementation 'androidx.appcompat:appcompat:1.1.0-rc01'
implementation 'androidx.swiperefreshlayout:swiperefreshlayout:1.1.0-alpha02'
```

This is `react-native-screens` related

@see [this og solution](https://github.com/react-navigation/react-navigation/issues/6267#issuecomment-528883756)


:heavy_check_mark: Raise `minSdkVersion = 21` (originally `16`)

```
buildscript {
    ext {
        buildToolsVersion = "28.0.3"
        minSdkVersion = 21 // <-- originally it's 16, which cause maxDex... issue
        compileSdkVersion = 28
        targetSdkVersion = 28
    }
    ...
}
```

### react-native-vector-icons

:heavy_check_mark: After installing `react-native-vector-icons`, add this line into `./android/app/build.gradle` (Usually at the end of the file):

```
apply from: "../../node_modules/react-native-vector-icons/fonts.gradle" // <--- add this
apply from: file("../../node_modules/@react-native-community/cli-platform-android/native_modules.gradle"); applyNativeModulesAppBuildGradle(project)
```

@see [this official docs](https://github.com/oblador/react-native-vector-icons#option-with-gradle-recommended)

## For iOS

### react-native-vector-icons

:heavy_check_mark: After installing `react-native-vector-icons`, follow [this official docs](https://github.com/oblador/react-native-vector-icons#option-manually)


## Premium Support

[Ignite CLI](https://infinite.red/ignite), [Ignite Bowser](https://github.com/infinitered/ignite-bowser), as open source projects, are free to use and always will be. [Infinite Red](https://infinite.red/) offers premium Ignite support and general mobile app design/development services. Email us at [hello@infinite.red](mailto:hello@infinite.red) to get in touch with us for more details.
