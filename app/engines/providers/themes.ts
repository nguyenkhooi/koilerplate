//* SAUCE imp
import { themeDark, themeLight } from "utils"

import AsyncStorage from "@react-native-community/async-storage"

import * as React from "react"

export const ThemeContext = React.createContext(null)
export function ThemeProvider(props: { theme: `themeDark` | `themeLight` }) {
  const { theme } = props
  const _themeColor = theme == `themeDark` ? themeDark : themeLight

  const [_colors, setColors] = React.useState(_themeColor)
  const [_dark, setDark] = React.useState(theme === "themeDark" ? true : false)
  // console.log('(TContext) _theme: ', _theme);
  React.useEffect(() => {
    switch (theme) {
      case "themeLight":
        setColors(themeLight)
        setDark(false)
        AsyncStorage.setItem("@preferences", JSON.stringify({ theme: "themeLight" }))
        break
      case "themeDark":
        setColors(themeDark)
        AsyncStorage.setItem("@preferences", JSON.stringify({ theme: "themeDark" }))
        break
      default:
        setColors(themeLight)
        setDark(false)
        AsyncStorage.setItem("@preferences", JSON.stringify({ theme: "themeLight" }))
        break
    }

    // AsyncStorage.getItem('@preferences', async (err, result) => {
    //   if (result == null) {
    //     console.log('(TContext) AsS after:  : no preferences');
    //   } else {
    //     const preferences = JSON.parse(result);
    //     console.log('(TContext) AsS after: ', preferences);
    //   }
    // });
  }, [theme])
  return (
    <ThemeContext.Provider
      value={{
        theme: {
          colors: _colors,
          dark: _dark,
        },
        setTheme: props.setTheme,
      }}
    >
      {props.children}
    </ThemeContext.Provider>
  )
}

export function withTheme(Component) {
  return function ThemeComponent(props) {
    return (
      <ThemeContext.Consumer>
        {contexts => <Component {...props} {...contexts} />}
      </ThemeContext.Consumer>
    )
  }
}
