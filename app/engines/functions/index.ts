import ramda from "ramda"

export const functions = {
  auth: require("./authFunctions"),
  others: require("./otherFunctions"),
  ramda: ramda,
}
