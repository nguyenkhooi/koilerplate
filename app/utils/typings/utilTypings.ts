import { colors } from "utils"
import { TextStyle, ViewStyle } from "react-native"

/** TODO 📕 this */
/**
 * @see https://stackoverflow.com/a/49286056
 */
export type ValueOf<T> = T[keyof T]

export type IPcolors = typeof colors
export type IPpalette = ValueOf<IPcolors>
/**
 * @description List of available typography used in the codebase. Should be strictly used based on system design
 *
 */
export interface IPtypoCarp {
  largeTitle: TextStyle
  headline: TextStyle
  title: TextStyle
  subtitle: TextStyle
  subtitleEmphasized: TextStyle
  body: TextStyle
  bodyEmphasized: TextStyle
  caption: TextStyle
  captionEmphasized: TextStyle
}

//* from `presets.ts`

/**
 * @description presets styles/ values according to the design system
 */
export interface IPkoiCarp {
  screen: ViewStyle
  footer: ViewStyle
}

//* from `theme`

export interface IPthemeCarp {
  colors: IPcolors
  dark?: boolean
}
